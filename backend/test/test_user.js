process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let User = require('../models/user').User;
let Usergroup = require('../models/user').Usergroup;
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('User', () => {
  beforeEach((done) => {
        User.remove({}, (err) => {
           done();
        });
  });
  describe('POST user', () => {
      it('POST user', (done) => {
        const group = new Usergroup({
          _id: "group1"
        });
        group.save((err, group) => {
          const user = {
            _id: "User1",
            password: "pass",
            group: "group1"
          }
          chai.request(server)
            .post('/user/signup')
            .send(user)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id');
                done();
            });
        });
      });

  });
  describe('POST user with non-existing group', () => {
      it('POST user and return 404', (done) => {
          const user = {
            _id: "User2",
            password: "pass",
            group: "DOES_NOT_EXIST"
          }
          chai.request(server)
            .post('/user/signup')
            .send(user)
            .end((err, res) => {
                res.should.have.status(404);
                done();
            });
      });

  });
  describe('PUT user', () => {
    it('PUT user', (done) => {
      const user = new User({
          _id: "User3",
          email: "a@b.com",
          group: "group1",
          password: "pass"
      });
      user.save((err, user) => {
          chai.request(server)
            .put('/user/' + user._id)
            .send({email: "aaa@bbb.com"})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('email').eql('aaa@bbb.com');
              done();
            });
        });
    });
  });
  describe('PUT user 404', () => {
    it('PUT user 404', (done) => {
          chai.request(server)
            .put('/user/DOES_NOT_EXIST')
            .end((err, res) => {
              res.should.have.status(404);
              done();
            });
    });
  });
/*
  describe('PUT user -- wrong group', () => {
    it('PUT user -- wrong group', (done) => {
      const user = new User({
          _id: "User3",
          email: "a@b.com",
          group: "group1",
          password: "pass"
      });
      user.save((err, user) => {
          chai.request(server)
            .put('/user/' + user._id)
            .send({group: "DOES_NOT_EXIST"})
            .end((err, res) => {
              res.should.have.status(200);
              done();
            });
        });
    });
  });
*/
});
