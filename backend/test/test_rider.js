'use strict';
process.env.NODE_ENV = 'test'; // disables authentication

let mongoose = require("mongoose");
let Rider = require('../models/rider').Rider;
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('Rider', () => {
  beforeEach((done) => {
        Rider.remove({}, (err) => {
           done();
        });
  });
  describe('GET rider', () => {
      it('GET rider', (done) => {
        chai.request(server)
            .get('/rider')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
              done();
            });
      });
  });
  describe('POST rider', () => {
      it('POST rider', (done) => {
        const rider = {
            _id: "Rider1",
        }
        chai.request(server)
            .post('/rider/')
            .send(rider)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id').eql(rider._id);
              done();
            });
      });

  });
  describe('PUT rider', () => {
    it('PUT rider', (done) => {
      const rider = new Rider({
          _id: "Rider3",
          nationality: "NAT1"
      });
      rider.save((err, rider) => {
          chai.request(server)
          .put('/rider/' + rider._id)
          .send({nationality: "NAT2", date_of_birth: "01-01-1970 12:00:00"})
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('nationality').eql('NAT2');
            //res.body.should.have.property('date_of_birth').eql('01-01-1970 12:00:00');
            done();
          });
        });
    });
  });
});
