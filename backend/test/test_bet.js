//process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

let Bet = require('../models/bet').Bet;
let Race = require('../models/race').Race;
let Rider = require('../models/rider').Rider;
let User = require('../models/user').User;
let Usergroup = require('../models/user').Usergroup;

var token = '';

chai.use(chaiHttp);

// TODO - test delete bet of not-owned race 

describe('Bet', function(done) {
  // Drop database
  before((done) => {
    mongoose.createConnection('mongodb://localhost/cyclebet', function(err, res){
      if (err) { console.log("Mongo err:"+err);}
      mongoose.connection.db.dropDatabase(function(err, res){
        if (err) { console.log("DROP err:"+err);}
        done();
      });
    });
  });
  before((done) => {
    mongoose.connection.db.dropDatabase(function(err, res){
        if (err) { console.log("DROP err:"+err);}
        done();
    });
  });
  // Create group
  before((done) => {
    const group = new Usergroup({ _id: "group" });
    group.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  // Create riders
  before((done) => {
    const rider = new Rider({ _id: "rider1" });
    rider.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  before((done) => {
    const rider = new Rider({ _id: "rider2" });
    rider.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  before((done) => {
    const rider = new Rider({ _id: "rider3" });
    rider.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  // Create races
  before((done) => {
    const race = new Race({ _id: "race1", date:"01-01-2020 00:00:00" });
    race.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  before((done) => {
    const race = new Race({ _id: "race2", date:"01-03-2020 00:00:00" });
    race.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  before((done) => {
    const race = new Race({ _id: "race_finished", date:"01-03-1907 00:00:00" });
    race.save((err, res) => { if (err) { console.log(err); } done(); });
  });
  // Create user
  before((done) => {
        const user = {_id: "user", password: "pass", group: "group"};
        chai.request(server)
          .post('/user/signup')
          .send(user)
          .end((err, res) => { done(); });
  });
  // Authenticate user
  before((done) => {
        chai.request(server)
          .post('/user/authenticate')
          .send({_id: "user", password: "pass"})
          .end((err, res) => {
            console.log("XXX body:"+JSON.stringify(res.body));
            token = res.body.token.split(' ')[1];
            done();
          });
  });

  describe('GET bet', () => {
      it('it should GET all the bets', (done) => {
        chai.request(server)
            .get('/bet')
            .set({"x-access-token": token})
            .end((err, res) => {
                if (err) { console.log("XXX GET:"+err); }
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
      });
  });

  describe('POST bet', () => {
      it('POST bet', (done) => {
        const bet = {
            user: "user",
            race: "race1",
            riders: ["rider1", "rider2"]
        }
        chai.request(server)
            .post('/bet/')
            .set({"x-access-token": token})
            .send(bet)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id');
              done();
            });
      });

  });
  describe('POST bet (rider 404)', () => {
      it('POST bet (rider 404)', (done) => {
        const bet = {
            user: "user",
            race: "race1",
            riders: ["DOES_NOT_EXIST"]
        }
        chai.request(server)
            .post('/bet/')
            .set({"x-access-token": token})
            .send(bet)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(404);
              done();
            });
      });

  });
  describe('POST bet (race 404)', () => {
      it('POST bet (race 404)', (done) => {
        const bet = {
            user: "user",
            race: "DOES_NOT_EXIST",
            riders: ["DOES_NOT_EXIST"]
        }
        chai.request(server)
            .post('/bet/')
            .set({"x-access-token": token})
            .send(bet)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(404);
              done();
            });
      });

  });
  describe('POST bet (not unique)', () => {
      it('POST bet (not unique)', (done) => {
        const bet = {
            user: "user",
            race: "race1",
            riders: ["rider1"]
        }
        chai.request(server)
            .post('/bet/')
            .set({"x-access-token": token})
            .send(bet)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(200);
              done();
            });
        // POST bet again with same user and race
        const bet2 = {
            user: "user",
            race: "race1",
            riders: ["rider2"]
        }
        chai.request(server)
            .post('/bet/')
            .set({"x-access-token": token})
            .send(bet2)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(500);
              done();
            });
      });

  });
  describe('PUT bet', () => {
    it('PUT bet', (done) => {
      const bet = new Bet({
          user: "user",
          race: "race1",
          riders: ["rider1", "rider2"],
      });
      bet.save((err, bet) => {
          if (err) throw err;
          chai.request(server)
          .put('/bet/' + bet._id)
          .set({"x-access-token": token})
          .send({race: "race2"})
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('race').eql('race2');
            done();
          });
        });
    });
  });

  describe('DELETE bet', () => {
    it('DELETE bet', (done) => {
      const bet = new Bet({
        user: "user",
        race: "race1",
        riders: ["rider1", "rider2"]
      });
      bet.save((err, bet) => {
        if (err) throw err;
        if (!bet) { console.log("XXX NOT BET"); }
        chai.request(server)
        .del('/bet/' + bet._id)
        .set({"x-access-token": token})
        .end( (err, res) => {
          res.should.have.status(200);
          done();
        });
      });
    });
  });
  describe('DELETE bet (finished race)', () => {
    it('DELETE bet (finished race)', (done) => {
      const bet = new Bet({
        user: "user",
        race: "race_finished",
        riders: ["rider1", "rider2"]
      });
      bet.save((err, bet) => {
        if (err) throw err;
        if (!bet) { console.log("XXX NOT BET"); }
        chai.request(server)
        .del('/bet/' + bet._id)
        .set({"x-access-token": token})
        .end( (err, res) => {
          res.should.have.status(500);
          done();
        });
      });
    });
  });
});
