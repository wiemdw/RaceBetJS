process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Race = require('../models/race').Race;
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);


describe('Race', () => {
  beforeEach((done) => {
        Race.remove({}, (err) => {
           done();
        });
  });
  describe('GET races', () => {
      it('GET races', (done) => {
        chai.request(server)
            .get('/race')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
              done();
            });
      });
  });
  describe('GET race 404', () => {
      it('GET race 404', (done) => {
        chai.request(server)
            .get('/race/DOES_NOT_EXIST')
            .end((err, res) => {
                res.should.have.status(404);
              done();
            });
      });
  });
  describe('GET race', () => {
    it('GET race', (done) => {
      const race = new Race({
          _id: "Race3",
          race_type: "cycling"
      });
      race.save((err, race) => {
          chai.request(server)
          .get('/race/' + race._id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id').eql(race._id);
            res.body.should.have.property('race_type').eql(race.race_type);
            done();
          });
        });
    });
  });
  describe('POST race', () => {
      it('POST race', (done) => {
        const race = {
            _id: "Race1",
        }
        chai.request(server)
            .post('/race/')
            .send(race)
            .end((err, res) => {
                console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id');
              done();
            });
      });

  });
  describe('PUT race', () => {
    it('PUT race', (done) => {
      const race = new Race({
          _id: "Race3",
          race_type: "cycling"
      });
      race.save((err, race) => {
          chai.request(server)
          .put('/race/' + race._id)
          .send({race_type: "mountainbike"})
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('race_type').eql('mountainbike');
            done();
          });
        });
    });
  });
});
