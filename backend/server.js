"use strict";
//process.env.NODE_ENV = 'test';

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const config = require('./config/database');

const userRoutes = require('./routes/user');
const groupRoutes = require('./routes/group');
const raceRoutes = require('./routes/race');
const riderRoutes = require('./routes/rider');
const betRoutes = require('./routes/bet');
const resultRoutes = require('./routes/result');

const port = process.env.PORT || 8080;

var app = express();

// get our request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// log to console
app.use(morgan('dev'));

// CORS support -- must be before passport + adding routes!
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, x-access-token');
  if (req.method === 'OPTIONS') {
    res.send(200);
  } else {
    next();
  }
});

// Use the passport package in our application
app.use(passport.initialize());

// demo Route (GET http://localhost:8080)
app.get('/', function(req, res) {
  res.send('Hello! The API is at http://localhost:' + port);
});

// connect to database
mongoose.connect(config.database);

// add routes
app.use('/user', userRoutes);
app.use('/group', groupRoutes);
app.use('/race', raceRoutes);
app.use('/rider', riderRoutes);
app.use('/bet', betRoutes);
app.use('/result', resultRoutes);

// disable 304 response (from cache) in development
app.disable('etag');

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({"error": {
        message: err.message,
        error: err
    }});
  });

}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({'error': {
        message: err.message,
        error: {}
    }});
});

// start the server
app.listen(port);
console.log('The betting can continue at http://localhost:' + port);

module.exports = app;
