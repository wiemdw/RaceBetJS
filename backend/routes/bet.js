"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var auth = require('../auth');
var Bet = require('../models/bet').Bet;
var Race = require('../models/race').Race;

var betRouter = express.Router();
betRouter.use(bodyParser.json());

var validate_bet_date = function(raceId, res, next) {
  // validate the race is not already finished
  var currentDate = new Date();
  Race.findById(raceId, function(err, race) {
    if (err) { return next(err); }
    if (!race) {
      return res.status(404)
        .send({ error: 'race not found: '+raceId});
    }
    if (currentDate > race.date) {
      return res.status(500)
        .send({ error: 'race already finished' });
    }
  });
};

betRouter.route('/')
  // Get bets for current user
  .get(auth.authenticateUser, function(req, res, next) {
    Bet.find({ user: req.user._id })
      .populate('race')
      .populate('riders')
      .exec(function(err, bet) {
        if (err) { return next(err); }
        return res.json(bet);
      });
  })
  // Create bet for current user
  .post(auth.authenticateUser, function(req, res, next) {
    // make sure the scores are empty when creating a new bet
    req.body.scores = [];
    // set the current user id
    req.body.user = req.user._id;
  
    // TODO validate number of riders matches group setting
  
    // validate the race is not already finished
    validate_bet_date(req.body.race, res, next);
  
    Bet.create(req.body, function(err, bet) {
      if (err) { return next(err); }
      return res.json(bet);
    });
  });

betRouter.route('/:betId')
  // Get bet (must belong to the current user)
  .get(auth.authenticateUser, function(req, res, next) {
    Bet.findById(req.params.betId)
       .populate('race')
       .populate('riders')
       .exec(function(err, bet) {
         if (err) { return next(err); }
         if (bet.user !== req.user._id) {
           return res.status(403)
             .json({ message: 'Bet ' + req.params.betId + ' not owned by ' + req.user._id });
         }
         return res.json(bet);
       });
  })
  // Update bet (must belong to the current user)
  .put(auth.authenticateUser, function(req, res, next) {
    Bet.findById(req.params.betId).populate('race').exec(function(err, bet) {
      if (err) { return next(err); }
      if (!bet) {
        return res.status(404)
          .json({ message: 'Bet with id ' + req.params.betId + ' not found.' });
      }
      if (bet.user !== req.user._id) {
        return res.status(403)
          .json({ message: 'Bet ' + req.params.betId + ' not owned by ' + req.user._id });
      }
  
      validate_bet_date(bet.race._id, res, next);
  
      // update document from request body
      // note: update of document _id is silently ignored
      var attr = null;
      for (attr in req.body) {
        bet[attr] = req.body[attr];
      }
  
      // validate and save
      bet.save(function(err, bet) {
        if (err) { return next(err); }
        return res.json(bet);
      });
    });
  })
  // Delete bet
  .delete(auth.authenticateUser, function(req, res, next) {
    // Bets for finished races cannot be deleted
    Bet.findById(req.params.betId).populate('race').exec(function(err, bet) {
      if (err) { return next(err); }
  
      // Bets for finished races cannot be deleted
      validate_bet_date(bet.race._id, res, next);
  
      // Bet must belong to current user
      if (bet.user !== req.user._id) {
        return res.status(403)
          .json({ message: 'Bet ' + req.params.betId + ' not owned by ' + req.user._id });
      }
  
      // Remove from database
      bet.remove(function(err, bet) {
        if (err) { return next(err); }
        return res.json(bet);
      });
    });
  });

module.exports = betRouter;
