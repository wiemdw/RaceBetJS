"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var auth = require('../auth');
var Rider = require('../models/rider').Rider;

var riderRouter = express.Router();
riderRouter.use(bodyParser.json());

// Creating and updating riders requires admin privileges

riderRouter.route('/')
  // Get all riders
  .get(function (req, res, next) {
    Rider.find({}, function (err, rider) {
      if (err) { return next(err); }
      return res.json(rider);
    });
  })
  // Create rider
  .post(auth.authenticateUser, auth.requireAdmin, function (req, res, next) {
    Rider.create(req.body, function (err, rider) {
      if (err) { return next(err); }
      return res.json(rider);
    });
  });

riderRouter.route('/:riderId')
  // Get rider
  .get(function (req, res, next) {
    Rider.findById(req.params.riderId, function (err, rider) {
      if (!rider) {
        return res.status(404)
                  .json({message: 'Rider with id ' + req.params.riderId + ' not found.'});
      }
      if (err) { return next(err); }
      return res.json(rider);
    });
  })
  // Update rider
  .put(auth.authenticateUser, auth.requireAdmin, function (req, res, next) {
    // Not using User.update or User.findOneAndUpdate as that skips validation
    Rider.findById(req.params.riderId, function(err, rider) {
      if(err) { return next(err); }
      if (!rider) {
        return res.status(404)
                  .json({message: 'Rider with id ' + req.params.riderId + ' not found.'});
      }
      // update document from request body
      // note: update of document _id is silently ignored
      var attr = null;
      for(attr in req.body) {
          rider[attr] = req.body[attr];
      }
      // validate and save
      rider.save(function(err, rider) {
          if(err) { return next(err); }
          return res.json(rider);
      });
    });
  });

module.exports = riderRouter;
