"use strict";
var express = require('express');
var jwt = require('jwt-simple');
var config = require('../config/database');
var User = require('../models/user').User;
var passport = require('passport');
var auth = require('../auth');
	
require('../config/passport')(passport);

var userRoutes = express.Router();

// create a new user account (POST http://localhost:8080/api/signup)
userRoutes.post('/signup', function(req, res, next) {
  if (!req.body._id || !req.body.password) {
    return res.status(400).json({error: 'Please pass name and password.'});
  } else {
    var user = new User(req.body);
    user.save(function(err, user) {
      if(err) { return next(err); }
      return res.json(user);
    });
  }
});

// authenticate a user (POST http://localhost:8080/user/authenticate)
userRoutes.post('/authenticate', function(req, res, next) {
  User.findById(req.body._id, function(err, user) {
    if (err) { return next(err); }
    if (!user) {
      return res.status(401).send('Authentication failed. User '+req.body._id+' not found.');
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.encode(user, config.secret);
          // return the information including token as JSON
          // FIXME
          return res.json({success: true, token: 'JWT ' + token});
        } else {
          return res.status(401).send('Authentication failed. Wrong password.');
        }
      });
    }
  });
});

// User ID methods
userRoutes.route('/:userId')
.get(auth.authenticateUser, function (req, res, next) {
  User.findById(req.params.userId)
      .exec(function (err, user) {
        if (err) { return next(err); }
        if (!user) {
          return res.status(404)
                .json({message: 'User with id ' + req.params.userId + ' not found.'});
        }
        return res.json(user);
      });
})
.put(auth.authenticateUser, function (req, res, next) {
  // Not using User.update or User.findOneAndUpdate as that skips validation
  User.findById(req.params.userId, function(err, user) {
    if(err) { return next(err); }

    if(!user) {
      return res.status(404)
                .json({message: 'User with id ' + req.params.userId + ' not found.'});
    }
    // update document from request body
    // note: update of document _id is silently ignored
    var attr = null;
    for(attr in req.body) {
      user[attr] = req.body[attr];
    }
    // validate and save
    user.save(function(err, user) {
      if(err) { return next(err); }
      return res.json(user);
    });
  });
});

var getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    }
  }
}

module.exports = userRoutes;
