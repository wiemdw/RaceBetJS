"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var auth = require('../auth');
var Usergroup = require('../models/user').Usergroup;

var usergroupRouter = express.Router();
usergroupRouter.use(bodyParser.json());

// Creating user groups requires admin privileges

usergroupRouter.route('/')
  .get(function (req, res, next) {
    Usergroup.find({}, function (err, usergroup) {
      if (err) { return next(err); }
      return res.json(usergroup);
    });
  })
  .post(auth.authenticateUser, auth.requireAdmin, function (req, res, next) {
    Usergroup.create(req.body, function (err, usergroup) {
      if (err) { return next(err); }
      return res.json(usergroup);
    });
  });

module.exports = usergroupRouter;
