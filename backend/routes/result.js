"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var auth = require('../auth');
var Race = require('../models/race').Race;
var Bet = require('../models/bet').Bet;
var async = require('async');

var resultRouter = express.Router();
resultRouter.use(bodyParser.json());

resultRouter.route('/race/:raceId')
  // Get all results for a race
  .get(auth.authenticateUser, auth.requireAdmin, function(req, res, next) {
    Bet.aggregate([
    // Filter bets for race
    { $match: {"race": req.params.raceId } },
    // Lookup race
    { $lookup: {"localField": "race",
                "from": "races",
                "foreignField": "_id",
                "as": "race"}
    },
    // Lookup user
    { $lookup: {"localField": "user",
                "from": "users",
                "foreignField": "_id",
                "as": "userinfo"}
    },
    { $unwind: "$userinfo" },
    // Lookup group
    { $lookup: {"localField": "userinfo.group",
                "from": "usergroups",
                "foreignField": "_id",
                "as": "group"}
    },
    { $project: {"_id": 1,
                 "scores": 1,
                 "riders": 1,
                 "race.result": 1,
                 "group.riders_per_bet": 1}
    }
    ], function(err, result) {
        if (err) { return next(err); }
        return res.json(result);
    });
  })
  .post(auth.authenticateUser, function(req, res, next) {
    var updated_bets = [];

    async.series([
      // Update race results
      function (callback) {
        Race.findById(req.params.raceId, function(err, race) {
          if(err) { return next(err); }
          if(!race) {
            return res.status(404)
              .json({message: 'Race with id ' + req.params.raceId + ' not found.'});
          }
          race.result = req.body.result;
          race.save(function(err, race) {
            if(err) { return next(err); }
            callback();
          });
        });
      },

      // Update all bet scores for this race
      function (callback) {
        Bet.aggregate([
          // Filter bets for race
          { $match: {"race": req.params.raceId, } },
          // Lookup race, user, group
          { $lookup: {"localField": "race",
                      "from": "races",
                      "foreignField": "_id",
                      "as": "race"} },
          { $lookup: {"localField": "user",
                      "from": "users",
                      "foreignField": "_id",
                      "as": "userinfo"} },
          { $unwind: "$userinfo" },
          { $lookup: {"localField": "userinfo.group",
                      "from": "usergroups",
                      "foreignField": "_id",
                      "as": "group"} },
          { $unwind: "$group" },
          // Filter result for groups that allow betting on the race
          { $project: {"_id": 1,
                       "scores": 1,
                       "riders": 1,
                       "race.result": 1,
                       "group.riders_per_bet": 1} }
        ], function(err, result) {
          if (err) { return next(err); }

          // loop results
          async.forEach(result, function(bet, callback) {
            var scores = new Array(bet.group.riders_per_bet).fill(0);

            // take first X riders from race.result
            var top_riders = req.body.result.slice(0, bet.group.riders_per_bet+1);
            for (var j=0; j<bet.riders.length; j++) {
                var rider = bet.riders[j];
                var rider_index = top_riders.indexOf(rider);
                // rider is in the result
                if (rider_index > -1) {
                  scores[j] = 1;
                }
                // rider is in the correct position in the result
                if (j == rider_index) {
                    scores[j] = scores[j] + bet.group.riders_per_bet - j;
                }
            }

            // save bet with scores
            Bet.findByIdAndUpdate(bet._id,
              { scores: scores },
              { new: true },
              function (err, bet) {
                if (err) { return next(err); }
                updated_bets.push(bet);
                callback();
            });
          }, function(err) {
            if (err) return next(err);
            return res.json(updated_bets);
          });
        });
      }
    ], function(err) {
      if (err) return next(err);
    });
  });

module.exports = resultRouter;
