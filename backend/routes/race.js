"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var auth = require('../auth');
var Race = require('../models/race').Race;

var raceRouter = express.Router();
raceRouter.use(bodyParser.json());

// Creating and updating races requires admin privileges

raceRouter.route('/')
  // Get all races
  .get(function (req, res, next) {
    Race.find({}, function (err, race) {
      if (err) { return next(err); }
      return res.json(race);
    });
  })
  // Create race
  .post(auth.authenticateUser, auth.requireAdmin, function (req, res, next) {
    Race.create(req.body, function (err, race) {
      if (err) { return next(err); }
      return res.json(race);
    });
  });


raceRouter.route('/:raceId')
  // Retrieve race
  .get(function (req, res, next) {
    Race.findById(req.params.raceId, function (err, race) {
      if (err) { return next(err); }
      if(!race) {
        return res.status(404)
                  .json({message: 'Race with id ' + req.params.raceId + ' not found.'});
      }
      return res.json(race);
    });
  })
  // Update race
  .put(auth.authenticateUser, auth.requireAdmin, function (req, res, next) {
    Race.findById(req.params.raceId, function(err, race) {
      if(err) { return next(err); }
  
      if(!race) {
        return res.status(404)
                  .json({message: 'Race with id ' + req.params.raceId + ' not found.'});
      }
      // update document from request body
      // note: update of document _id is silently ignored
      var attr = null;
      for(attr in req.body) {
        race[attr] = req.body[attr];
      }
      // validate and save
      race.save(function(err, race) {
        if(err) { return next(err); }
        return res.json(race);
      });
    });
  });

module.exports = raceRouter;
