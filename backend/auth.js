"use strict";
var jwt = require('jsonwebtoken');
var config = require('./config/database');

exports.getToken = function (user) {
  return jwt.sign(user, config.secret, { expiresIn: 3600 });
};

exports.authenticateUser = function (req, res, next) {
  // skip authentication for test environments
  if (process.env.NODE_ENV === 'test') { return next(); }

  // check header or url parameters or post parameters for token -- TODO
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, config.secret, function (err, decoded) {
      if (err) {
        err = new Error('You are not authenticated!');
        err.status = 401;
        return next(err);
      } else {
        // if everything is good, save to request for use in other routes
        req.user = decoded;
        return next();
      }
    });
  } else {
    // if there is no token: return an error
    var err = new Error('No authentication token provided!');
    err.status = 403;
    return next(err);
  }
};

exports.requireAdmin = function(req, res, next) {
  // skip authentication for test environments
  if (process.env.NODE_ENV === 'test') { return next(); }

  if (req.user.admin) {
    return next();
  } else {
    var err = new Error('This action requires admin privileges');
    err.status = 403;
    return next(err);
  }
};
