'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var raceSchema = new Schema({
    _id: {type:String, required:true, unique:true, alias:'name'},
    date: {type:Date, required:true},
    race_type: {type:String},
    country: {type:String},
    state: {type:String, enum:['OK', 'Cancelled']},
    result: [{type:String, ref:'Rider'}]
});

// validate riders in race result are existing documents
raceSchema.pre('save', function(next) {
    if (this.isModified('result')) {
      var Rider = mongoose.model('Rider');
      for (var i = 0; i < this.result.length; i++) {
          var rider = this.result[i];
          Rider.findById(rider, function (err, found) {
              if (!found) {
                  err = new Error('rider not found: '+rider);
                  err.status = 404;
                  return next(err);
              } else if (err) {
                  return next(err);
              } else {
                  return next();
              }
          });
      }
    } else {
      return next();
    }
});

module.exports = {
    Race: mongoose.model('Race', raceSchema),
};
