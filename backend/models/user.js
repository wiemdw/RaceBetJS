'use strict';

var bcrypt = require('bcrypt');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Thanks to http://blog.matoski.com/articles/jwt-express-node-mongoose/

var UsergroupSchema = new Schema({
  _id: {type:String, required:true, unique:true, alias:'name'},
  races_list: [{ type:String, ref:'Race' }],
  riders_per_bet: {type:Number, default:3}
});

var UserSchema = new Schema({
  _id: { type: String, unique: true, required: true, alias: 'name' },
  password: { type: String, required: true },
  email: { type: String },
  group: { type:String, ref:'Usergroup', required: true },
  admin: { type: Boolean, default: false }
});

// validate password
UserSchema.pre('save', function (next) {
    var user = this;

    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function(err,salt){
            if(err) { return next(err); }
            bcrypt.hash(user.password, salt, function(err,hash){
               if(err) { return next(err); }
               user.password = hash;
               return next();
            });
        });
    }
});

// validate group
UserSchema.pre('save', function (next) {
    var user = this;

    if (this.isModified('group') || this.isNew) {
         var Usergroup = mongoose.model('Usergroup');
         Usergroup.findById(user.group, function (err, found) {
             if (!found) {
                err = new Error('group not found:'+user.group);
                err.status = 404;
                return next(err);
             } else if (err) {
                return next(err);
             } else {
                return next();
             }
         });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) { return cb(err); }
        cb(null, isMatch);
    });
};

module.exports = {
    Usergroup: mongoose.model('Usergroup', UsergroupSchema),
    User: mongoose.model('User', UserSchema)
};
