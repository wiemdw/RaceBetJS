'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// TODO - created_at / updated_at instead of bet.date ??
var betSchema = new Schema({
  date: { type: Date, default: Date.now },
  user: { type: String, ref: 'User', required: true },
  race: { type: String, ref: 'Race', required: true },
  riders: [{ type: String, ref: 'Rider' }],
  scores: [ Number ],
});

// validate race is an existing document
betSchema.pre('save', function(next) {
  if (this.isModified('race') || this.isNew) {
      var Race = mongoose.model('Race');
      Race.findById(this.race, function (err, found) {
        if (err) {
            return next(err);
        } else if (!found) {
            err = new Error('race not found');
            err.status = 404;
            return next(err);
        } else {
            return next();
        }
      });
  } else {
    return next();
  }
});

// validate riders are existing documents
betSchema.pre('save', function(next) {
  var bet = this;

  if (this.isModified('riders') || this.isNew) {
    var Rider = mongoose.model('Rider');
    for (var i = 0; i < bet.riders.length; i++) {
      Rider.findById(bet.riders[i], function (err, found) {
        if (err) {
            return next(err);
        } else if (!found) {
            err = new Error('rider not found: '+bet.riders[i]);
            err.status = 404;
            return next(err);
        } else {
            return next();
        }
      });
    }
  } else {
    return next();
  }
});

// Only one bet per user-race combination allowed
betSchema.index({ user: 1, race: 1 }, { unique: true });

module.exports = {
  Bet: mongoose.model('Bet', betSchema),
};
