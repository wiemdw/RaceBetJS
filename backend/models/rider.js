'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var riderSchema = new Schema({
    _id: {type:String, required:true, unique:true, alias:'name'},
    date_of_birth: {type:Date},
    nationality: {type:String},
    team: {type:String},
    state: {type:String, enum:['Available','Suspended','Injured',
                               'Retired','Not Available']}
});

module.exports = {
    Rider: mongoose.model('Rider', riderSchema),
};
