var mongoose = require('mongoose'),
    assert = require('assert');

var Rider = require('./models/rider').Rider;
var Race = require('./models/race').Race;
var User = require('./models/user').User;
var Group = require('./models/user').Group;
var Bet = require('./models/bet').Bet;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Connection URL
var url = 'mongodb://localhost:27017/cyclebet';
var conn = mongoose.connect(url);

mongoose.connection.on('open', function(){
    conn.connection.db.dropDatabase(function(err, result){
        sleep(1000);
    });
});

var db = conn.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");

    // create races
    Race.create({
        _id: "RVV 2018",
        date: "2018-04-01",
        country: "Belgium",
        state: "OK"
    }, function (err, race) {
        if (err) throw err;
        race.result.push("NotExistingRider");
        race.save(function (err, race) {
            if (err) console.log('rider_not_found:'+err);
        });
    });

    Bet.create({
        user: "user",
        race: "DoesNotExist"
    }, function (err, res) {
        if (err) {
            for (field in err.errors) {
                throw err.errors[field];
            }
        }
        if (err) console.log('race_not_found:'+err);
    });
 
    Bet.create({
        user: "user",
        race: "RVV 2018",
        riders: ["NotARider"] 
    }, function (err, res) {
        if (err) {
            for (field in err.errors) {
                throw err.errors[field];
            }
        }
        if (err) console.log('rider_not_found:'+err);
    });

    User.create({
        name: "test",
        password: "test",
        group: "404",
    }, function (err, res) {
        if (err) {
            for (field in err.errors) {
                throw err.errors[field];
            }
        }
        if (err) console.log('group_not_found:'+err);
    });
});
