var mongoose = require('mongoose'),
    assert = require('assert');

var Rider = require('../models/rider').Rider;
var Race = require('../models/race').Race;
var User = require('../models/user').User;
var Group = require('../models/user').Group;
var Bet = require('../models/bet').Bet;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Connection URL
var url = 'mongodb://localhost:27017/cyclebet';
var conn = mongoose.connect(url);

mongoose.connection.on('open', function(){
    conn.connection.db.dropDatabase(function(err, result){
        if (err) throw err;
        console.log("drop db: " + result);
        sleep(1000);
    });
});

var db = conn.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");
    // create a new riders 
    Rider.create({
        _id: "Greg Van Avermaet",
        date_of_birth: "1979-01-02",
        nationality: "Belgian",
        team: "BMC",
        state: "Available" 
    }, function (err, rider) {
        if (err) throw err;
        console.log(rider._id);
        sleep(2000);
    });
    Rider.create({
        _id: "Philippe Gilbert",
        date_of_birth: "1978-01-02",
        nationality: "Belgian",
        team: "Quick-Step Floors",
        state: "Available" 
    }, function (err, rider) {
        if (err) throw err;
        console.log(rider._id);
        sleep(2000);
    });
    Rider.create({
        _id: "Chris Froome",
        date_of_birth: "1977-11-02",
        nationality: "UK",
        team: "Sky",
        state: "Available" 
    }, function (err, rider) {
        if (err) throw err;
        console.log(rider._id);
        sleep(2000);
    });

    // create races
    Race.create({
        _id: "RVV 2018",
        date: "2018-04-01",
        country: "Belgium",
        state: "OK"
    }, function (err, race) {
        if (err) throw err;
        console.log(race._id);
        sleep(2000);
    });
    Race.create({
        _id: "LBL 2016",
        date: "2016-04-01",
        country: "Belgium",
        state: "OK"
    }, function (err, race) {
        if (err) throw err;
        console.log(race._id);
        sleep(2000);
    });
    Race.create({
        _id: "TDF 2017",
        date: "2017-06-01",
        country: "France",
        state: "OK"
    }, function (err, race) {
        if (err) throw err;
        console.log(race._id);
        sleep(2000);
    });
    Race.create({
        _id: "RondeGaatNietDoor",
        date: "2017-12-01",
        country: "Belgium",
        state: "Cancelled"
    }, function (err, race) {
        if (err) throw err;
        console.log(race._id);
        sleep(2000);
    });
    // create groups
    Group.create({
        _id: "Ampli",
        races_list: ["RVV 2018", "LBL 2016", "TDF 2017", "RondeGaatNietDoor"],
        riders_per_bet: 3
    }, function (err, res) {
        if (err) {
            for (field in err.errors) {
                throw err.errors[field];
            }
        }
        console.log(res._id);
        sleep(2000);
    });
    Group.create({
        _id: "Norace",
        races_list: [],
    }, function (err, res) {
        if (err) throw err;
        console.log(res._id);
        sleep(2000);
    });

    console.log("Groups:");
    Group.find({}, function(err, res) {
        console.log(res);
    });
    console.log('End groups');

    // create users
    User.create({
        name: "wiem",
        password: "wiem",
        group: "Ampli",
        admin: false 
    }, function (err, res) {
        if (err) {
            for (field in err.errors) {
                throw err.errors[field];
            }
        }
        //if (err) throw err;
        console.log(res._id);
        sleep(2000);
    });
    User.create({
        name: "jos",
        password: "jos",
        group: "Ampli",
        admin: false
    }, function (err, res) {
        if (err) throw err;
        console.log(res._id);
        sleep(2000);
    });
    User.create({
        name: "admin",
        password: "admin",
        group: "Norace",
        admin: true 
    }, function (err, res) {
        if (err) {
            for (field in err.errors) {
                throw err.errors[field];
            }
        }
        console.log(res._id);
        sleep(2000);
    });
});
