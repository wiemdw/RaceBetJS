import time
import json
import requests
from bs4 import BeautifulSoup

riders = []

def get_teams(riders, url):
    url = 'http://www.wvcycling.com/en/teams/2017/wt/'
    soup = BeautifulSoup(requests.get(url).text)
    tr = soup.find("div", id="ploegen")
    for i, li in enumerate(tr.find_all('li')):
        team_url = li.find('a').attrs['href']
        team = li.get_text().strip()
        print team, team_url
        team_riders_url = team_url.replace('/2017/', 'riders/2017/')
        get_riders(riders, team, team_riders_url)

def get_riders(riders, team, team_riders_url):
    soup = BeautifulSoup(requests.get(team_riders_url).text)
    tr = soup.find("div", id="grid")
    for i, li in enumerate(tr.find_all('li')):
        if i == 0:
            continue # header
        name = li.find('div', class_="ploegrenner").find('a').get_text()
        nat = li.find('div', class_='flag').find('img').attrs['alt']
        dob = li.find('div', class_="datum").get_text()
        riders.append({"_id": name, "nationality": nat, "team": team, "dob": dob})

#get_teams(riders, '')
#print "riders: %s" % len(riders)
#with open('seed_data/riders.json', 'w') as fd:
#    json.dump(riders, fd)


def get_races():
    races = []
    links = {}
    for i in xrange(1, 13):
        url = 'http://www.wvcycling.com/en/calendarroad/2017/%02d/ME/' % (i)
        print "races url: %s" % url
        soup = BeautifulSoup(requests.get(url).text)
        tr = soup.find("div", id="grid")
        for idx, li in enumerate(tr.find_all('li')):
            country = li.find('div', class_='flag').find('img').attrs['alt']
            name = li.find('div', class_="wedstrijd").find('a').get_text()
            day = li.find('div', class_="dag").get_text()
            date = '%s-%02d-2017' % (day, i)
            races.append({"_id":name, "country":country, "date": date})
            
            link = li.find('div', class_="wedstrijd").find('a').attrs['href']
            links[name] = link
            
    return (races, links)

races, links = get_races()
print "races: %s" % len(races)
#with open('seed_data/races.json', 'w') as fd:
#    json.dump(races, fd)
with open("seed_data/race_links.json", "w") as fd:
    json.dump(links, fd, indent=2)

def get_results(links, max_results=10):
    results = dict()
    for rname, link in links.iteritems():
        time.sleep(10)
        result_url = link.replace('info', 'result')
        print "results: %s" % result_url
        soup = BeautifulSoup(requests.get(result_url).text)
        tr = soup.find("div", id="grid")
        for i, li in enumerate(tr.find_all('li')):
            name = li.find('div', class_="renneruitslag").find('a').get_text()
            results.setdefault(rname, []).append(name)
            if i == max_results:
                break
        
    return results

results = get_results(links)
print "results: %s" % len(results)
with open('seed_data/results.json', 'w') as fd:
    json.dump(results, fd)
