var should = require('should')
  , DB = require('../../config/database')
  , fixtures = require('../fixtures/model-rider')

var Rider = require('../../models/rider').Rider

describe('Model Rider Tests', function() {

  before(function(done) {
    DB.connect(DB.MODE_TEST, done)
  })

  beforeEach(function(done) {
    DB.drop(function(err) {
      if (err) return done(err)
      DB.fixtures(fixtures, done)
    })
  })

  it('all', function(done) {
    Rider.all(function(err, riders) {
      riders.length.should.eql(2)
      done()
    })
  })

  it('create', function(done) {
    Rider.create('Rider 3', function(err, id) {
      Rider.all(function(err, riders) {
        riders.length.should.eql(3)
        riders[3]._id.should.eql(id)
        //comments[3].user.should.eql('Famous Person')
        //comments[3].text.should.eql('I am so famous!')
        done()
      })
    })
  })

  it('remove', function(done) {
    Rider.all(function(err, riders) {
      Rider.remove(riders[0]._id, function(err) {
        Rider.all(function(err, result) {
          result.length.should.eql(2)
          result[0]._id.should.not.eql(riders[0]._id)
          result[1]._id.should.not.eql(riders[0]._id)
          done()
        })
      })
    })
  })
})
