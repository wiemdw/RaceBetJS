import sys
import json
import requests
import datetime
import urllib
from dateutil import parser
from requests_jwt import JWTAuth
from pprint import pprint

wait = False 

from pymongo import MongoClient
client = MongoClient()
client.drop_database('cyclebet')

db = client.cyclebet
db.usergroups.insert_one({"_id": "admin"})

URL = "http://localhost:8080"
auth = None
headers = {'Content-type': 'application/json'}
tokens = dict()

user = {'_id': 'admin', 'password': 'admin', 'group': 'admin', 'admin': True}
res = requests.post(URL+'/user/signup/', data=json.dumps(user), headers=headers, auth=auth)
if res.status_code != 200:
    print res.status_code, res.text

res = requests.post(URL+'/user/authenticate/', data=json.dumps(user), headers=headers).text
tokens['admin'] = json.loads(res)['token']
headers['x-access-token'] = tokens['admin'].split(' ')[1]
print "admin token: %s" % headers['x-access-token']
if wait:
    raw_input("<<<<<<<<<<<<<")
######################################################################
print "Add groups"
######################################################################
group1 = {'_id': 'group1'}
res = requests.post(URL+'/group/', data=json.dumps(group1), auth=auth, headers=headers)
print res.status_code, res.text
group2 = {'_id': 'group2'}
res = requests.post(URL+'/group/', data=json.dumps(group2), auth=auth, headers=headers)
print res.status_code, res.text
if wait:
    raw_input("<<<<<<<<<<<<<")
#######################################################################
print "Add users"
#######################################################################
for i in xrange(1,5):
    group = 'group1' if i % 2 == 0 else 'group2'
    user = {'_id': 'user%i' % i, 'password': 'pass','group': group}
    res = requests.post(URL+'/user/signup/', data=json.dumps(user), headers=headers, auth=auth)
    print 'user%i' % i, res.status_code, res.text

    res = requests.post(URL+'/user/authenticate/', data=json.dumps(user), headers=headers).text
    tokens[user['_id']] = json.loads(res)['token']
if wait:
    raw_input("<<<<<<<<<<<<<")

# TODO - PUT user
######################################################################
print "Add riders"
######################################################################
with open('seed_data/riders.json') as fd:
    riders = json.loads(fd.read())
for rider in riders:
    res = requests.post(URL+'/rider/', data=json.dumps(rider), auth=auth, headers=headers)
    if res.status_code != 200:
        print rider['_id'], res.status_code, res.text
if wait:
    raw_input("<<<<<<<<<<<<<")
######################################################################
print "Add races"
######################################################################
with open('seed_data/races.json') as fd:
    races = json.loads(fd.read())
print len(races)
for race in races:
    date = datetime.datetime.strptime(race['date'], '%d-%m-%Y') + datetime.timedelta(days=1000)
    race['date'] = date.strftime('%Y/%m/%d')
    res = requests.post(URL+'/race/', data=json.dumps(race), auth=auth, headers=headers)
    if res.status_code != 200:
        print race['_id'], res.status_code, res.text
if wait:
    raw_input("<<<<<<<<<<<<<")
####################################################################

bets = dict()
headers['x-access-token'] = tokens['user1'].split(' ')[1]

####################################################################
print "# RACE NOT FOUND"
bet = {"race": "NOTFOUND",
       "riders": ["Philippe  Gilbert", "Greg  Van Avermaet", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 404:
    print "POST bet should fail with 404: %s %s" % (res.status_code, res.text)
####################################################################
print "# RIDER NOT FOUND"
bet = {"race": "Ronde van Vlaanderen",
       "riders": ["NOTFOUND", "Greg  Van Avermaet", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 404:
    print "POST bet should fail with 404: %s %s" % (res.status_code, res.text)
####################################################################
print "# DELETE BET"
bet = {"race": "Ronde van Vlaanderen",
       "riders": ["Philippe  Gilbert", "Greg  Van Avermaet", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 200:
    bet_id = json.loads(res.text)['_id']
    res = requests.delete(URL+'/bet/'+bet_id, auth=auth, headers=headers)
    if res.status_code != 200:
        print "Failed to delete bet: %s %s" % (res.status_code, res.text)
####################################################################
print "# DUPLICATE BET"
bet = {"race": "Milano-Torino",
       "riders": ["Philippe  Gilbert", "Greg  Van Avermaet", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 500:
    print "Duplicat bet should fail: %s %s" % (res.status_code, res.text)
####################################################################
print "# CREATE BETS"

# Correct bet 9 points
bet = {"race": "Ronde van Vlaanderen",
       "riders": ["Philippe  Gilbert", "Greg  Van Avermaet", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 200:
    print bet, res.status_code, res.text
bets['correct'] = json.loads(res.text)

# Wrong order 6 points 
bet = {"race": "Ronde van Vlaanderen",
       "riders": ["Philippe  Gilbert", "Niki  Terpstra", "Greg  Van Avermaet"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 200:
    print bet, res.status_code, res.text
bets['order'] = json.loads(res.text)

# Partial bet 6 points
bet = {"race": "Ronde van Vlaanderen",
       "riders": ["Philippe  Gilbert", "Christopher  Froome", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user2'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 200:
    print bet, res.status_code, res.text
bets['partial'] = json.loads(res.text)

# Wrong bet 0 points
bet = {"race": "Ronde van Vlaanderen",
       "riders": ["Christopher  Froome", "Peter  Sagan", "Luke  Durbridge"]}
headers['x-access-token'] = tokens['user3'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 200:
    print bet, res.status_code, res.text
bets['wrong'] = json.loads(res.text)

# Other race bet 0 points
bet = {"race": "Sundvolden GP",
       "riders": ["Philippe  Gilbert", "Greg  Van Avermaet", "Niki  Terpstra"]}
headers['x-access-token'] = tokens['user1'].split(' ')[1]
res = requests.post(URL+'/bet/', data=json.dumps(bet), auth=auth, headers=headers)
if res.status_code != 200:
    print bet, res.status_code, res.text
bets['other'] = json.loads(res.text)

pprint(bets)

rtc = None
headers['x-access-token'] = tokens['admin'].split(' ')[1]
for race in json.loads(requests.get(URL+'/race/', headers=headers).text):
    rtc = rtc or race['_id']
    o_date = race['date']
    date = parser.parse(race['date']) -  datetime.timedelta(days=1000)
    race['date'] = date.strftime('%Y/%m/%d')
    res = requests.put(URL+'/race/'+race['_id'].replace('/', '%2F'), data=json.dumps(race), headers=headers)
    if res.status_code != 200:
        print race, res.status_code, res.text
    if race['_id'] == rtc:
        print race['_id'], o_date, race['date']

rtc = json.loads(requests.get(URL+'/race/'+rtc.replace('/', '%2F'), headers=headers).text)
print rtc['_id'], rtc['date']

print "Reading results"
with open('seed_data/results.json') as fd:
    results = json.loads(fd.read())
print len(results)
result = results['Ronde van Vlaanderen'][:3]
print result

print "Get result"
res = requests.get(URL+'/result/race/Ronde van Vlaanderen/', auth=auth, headers=headers)
pprint(res.text)

print "Post result"
res = requests.post(URL+'/result/race/Ronde van Vlaanderen/', data=json.dumps({'result': result}), auth=auth, headers=headers)
print res.status_code, res.text
#bets = json.loads(res.text)
for es, bet in bets.iteritems():
    headers['x-access-token'] = tokens[bet['user']].split(' ')[1]
    rbet = json.loads(requests.get(URL+'/bet/'+bet['_id'], auth=auth, headers=headers).text)
    print es, rbet['scores'], sum(rbet['scores'])

headers['x-access-token'] = tokens['admin'].split(' ')[1]
race = requests.get(URL+'/race/Ronde van Vlaanderen/', auth=auth, headers=headers)
print "Race result: %s" % json.loads(race.text)['result']

# TODO - GET /bet/ only returns current user's bets

