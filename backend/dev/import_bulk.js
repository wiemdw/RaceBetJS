var mongoose = require('mongoose'),
    assert = require('assert');

var Rider = require('../models/rider').Rider;
var Race = require('../models/race').Race;
var User = require('../models/user').User;
var Usergroup = require('../models/user').Usergroup;
var Bet = require('../models/bet').Bet;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Connection URL
var url = 'mongodb://localhost:27017/cyclebet';
var conn = mongoose.connect(url);
/*
mongoose.connection.on('open', function(){
    conn.connection.db.dropDatabase(function(err, result){
        if (err) throw err;
        console.log("drop db: " + result);
        sleep(1000);
    });
});
*/
var db = conn.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");
/*
    // create a new riders 
    Rider.create([{
        _id: "Greg Van Avermaet", date_of_birth: "1979-01-02",
        nationality: "Belgian", team: "BMC", state: "Available" 
    },{
        _id: "Philippe Gilbert", date_of_birth: "1978-01-02",
        nationality: "Belgian", team: "Quick-Step Floors",
        state: "Available" 
    },{
        _id: "Chris Froome", date_of_birth: "1977-11-02",
        nationality: "UK", team: "Sky", state: "Available" 
    }], function (err, res) {
        if (err) throw err;
        console.log(res);
    });

    // create races
    Race.create([{
        _id: "RVV 2018", date: "2018-04-01", country: "Belgium", state: "OK"
    },{
        _id: "LBL 2016", date: "2016-04-01", country: "Belgium", state: "OK"
    },{
        _id: "TDF 2017", date: "2017-06-01", country: "France", state: "OK"
    },{
        _id: "RondeGaatNietDoor", date: "2017-12-01", country: "Belgium", state: "Cancelled"
    }], function (err, res) {
        if (err) throw err;
        console.log(res);
    });
    // create groups
    Usergroup.create([{
        _id: "Ampli",
        races_list: ["RVV 2018", "LBL 2016", "TDF 2017", "RondeGaatNietDoor"],
        riders_per_bet: 3
    },{
        _id: "Norace",
        races_list: [],
    }], function (err, res) {
        if (err) throw err;
        console.log(res);
    });
*/
    // create users
    User.create([{
        _id: "wiem", password: "wiem", group: "Ampli", admin: false 
    },{ 
        _id: "jos", password: "jos", group: "Ampli", admin: false
    },{
        _id: "admin", password: "admin", group: "Norace", admin: true 
    }], function (err, res) {
        if (err) throw err;
        console.log(res);
    });
    // create bets
    Bet.create([{
        user: "wiem", race: "TDF 2017", riders:["Chris Froome"]
    },{
        user: "jos", race: "RVV 2018", riders:["Chris Froome", "Greg Van Avermaet"]
    }], function (err, res) {
        if (err) throw err;
        console.log(res);
    });

});
