'use strict';

angular.module('raceBet', ['ui.router', 'ngResource', 'ngStorage'])
   .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }
            })
            .state('app.bet', {
                url:'bet',
                views: {
                    'content@': {
                        templateUrl : 'views/bet.html',
                        controller  : 'BetController'                  
                    }
                }
            })
            .state('app.result', {
                url:'result',
                views: {
                    'content@': {
                        templateUrl : 'views/result.html',
                        controller  : 'ResultController'                  
                    }
                }
            })
            .state('app.profile', {
                url: 'profile',
                views: {
                    'content@': {
                        templateUrl : 'views/profile.html',
                        controller  : 'UserController'
                    }
                }
            });

        $urlRouterProvider.otherwise('/');

        $httpProvider.interceptors.push(['$q', '$location', '$localStorage', 
            function($q, $location, $localStorage) 
        {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                        // TODO - double token header
                        config.headers['x-access-token'] = $localStorage.token;
                    }
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        // TODO
                        $location.path('/signin');
                    }
                    return $q.reject(response);
                }
            };
        }]);
    })
;
