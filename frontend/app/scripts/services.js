'use strict';

angular.module('raceBet')
    .constant("baseURL","http://localhost:8080/")
    .service('betFactory', ['$resource', 'baseURL', function($resource, baseURL) {
        this.getBets = function(){
            return $resource(baseURL+"bet"); 
        };
        this.getBet = function(id) {
            return $resource(baseURL+"bet/:id", 
                             {},
                             {'delete':{method:'DELETE', params: {id: '@id'}}}); 
        };
    }])
    .service('resultFactory', ['$resource', 'baseURL', function($resource, baseURL) {
        this.getResults = function(){
            return $resource(baseURL+"result"); 
        };
    }])
    .service('userFactory', ['$resource', 'baseURL', '$localStorage',
        function($resource, baseURL, $localStorage) 
    {
        this.getProfile = function() {
            var userId = $localStorage.currentUser._id;
            console.log("calling /user with id "+userId+" type:"+typeof userId);
            return $resource(baseURL+"user/"+userId, null, {'update':{method:'PUT'}});
        };
    }])
    .factory('AuthFactory', ['$http', 'baseURL', '$localStorage', 
        function($http, baseURL, $localStorage)
    {
        function changeUser(user) { angular.extend(currentUser, user); }
 
        function urlBase64Decode(str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output);
        }
 
        function getUserFromToken() {
            var token = $localStorage.token;
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                user = JSON.parse(urlBase64Decode(encoded));
            }
            return user;
        }
 
        var currentUser = getUserFromToken();
 
        return {
            register: function(data) {
                return $http.post(baseURL + 'user/signup', data)
                    .then(
                        function(response){
                            $localStorage.currentUser = currentUser;
                            return response.data; 
                        },
                        function(response){ 
                            return response.data;
                        }
                    );
            },
            authenticate: function(data) {
                return $http.post(baseURL + 'user/authenticate', data)
                    .then(
                        function(response) {
                            console.log("authenticated:"+ JSON.stringify(response.data));
                            $localStorage.currentUser = currentUser;
                            console.log("current user:"+ JSON.stringify($localStorage.currentUser));
                            return response.data; 
                        },
                        function(response) {
                            console.log("not authenticated:"+ JSON.stringify(response));
                            // TODO - convert to error
                            return response.data;
                        }
                    );
            },
            logout: function() {
                changeUser({});
                delete $localStorage.token;
            }
        };
    }]);
;
