'use strict';


angular.module('raceBet')
    // BetController
    .controller('BetController', ['$scope', 'betFactory', function($scope, betFactory) {
        $scope.bets = betFactory.getBets().query();

        $scope.updateBet = function() {
            console.log($scope.bet);
            betFactory.getBets().update($scope.bet);
        };
        $scope.addBet = function() {
            console.log($scope.new_bet);
            new_bet = betFactory.getBets().save($scope.new_bet);
            $scope.bets.push(new_bet);
        };
        $scope.deleteBet = function(betId) {
            betFactory.getBet().delete({id: betId});
        }; 
    }])

    // ResultController
    .controller('ResultController',['$scope','resultFactory', function($scope,resultFactory) {
        $scope.results = resultFactory.getResults().query();
    }])

    // UserController
    .controller('UserController', ['$scope', 'userFactory', function($scope, userFactory) {
        $scope.profile = userFactory.getProfile().get();

        $scope.updateProfile = function() {
            console.log("updating profile"); 
            console.log($scope.profile);
            userFactory.getProfile().update($scope.profile);
        };
    }])

    // Login controller: register, authenticate, logout
    .controller('LoginController', 
        ['$rootScope', '$scope', '$location', '$localStorage', 'AuthFactory', 
        function($rootScope, $scope, $location, $localStorage, AuthFactory) 
    {
        $scope.authenticate = function(credentials)
        {
            var formData = {_id: credentials.name, password: credentials.password};
            AuthFactory.authenticate(formData).then(
                function(res) {
                    if (res.success == true) {
                        $localStorage.token = res.token.split(' ')[1];
                        $rootScope.currentUser = $localStorage.currentUser;
                    } else {
                        console.log("XXX ERR2: "+res);
                    }
                },
                function(res) {
                    console.log("Failed to authenticate");
                    //$rootScope.error = 'Failed to authenticate';
                }
            );
        };
 
        $scope.register = function(credentials) {
            var formData = {_id: credentials.name,
                            email: credentials.email,
                            group: credentials.group,
                            password: credentials.password};
 
            AuthFactory.register(formData).then(
                function(res) {
                    if (res.success == true) {
                        $localStorage.token = res.token.split(' ')[1];
                    } else {
                        alert(res.data);
                    }
                },
                function(res) {
                    console.log("here");
                    $rootScope.error = 'Failed to register';
                }
            );
        };
 
        $scope.logout = function() {
            AuthFactory.logout();
            $rootScope.currentUser = null; 
            window.location = "/"
        };
        $scope.token = $localStorage.token;
    }])
;
